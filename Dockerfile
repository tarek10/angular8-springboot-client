FROM nginx:latest
COPY dist/angular8-springboot-client/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
